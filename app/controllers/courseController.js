//Import thư viện Mongoose
const mongoose = require("mongoose");
//Import course model
const courseModel = require("../model/courseModel");

//function create course
const createCourse = (request,response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    console.log(body);
    //B2: validate dữ liệu
    if(!body.courseCode){
        return response.status(400).json({
            status: "Bad request",
            message: "Mã Course không hợp lệ"
        })
    }
    if(!body.courseName){
        return response.status(400).json({
            status: "Bad request",
            message: "Tên Course không hợp lệ"
        })
    }
    if(isNaN(body.price)||body.price < 0){
        return response.status(400).json({
            status: "Bad request",
            message: "Price không hợp lệ"
        })
    }
    if(isNaN(body.discountPrice)||body.discountPrice < 0){
        return response.status(400).json({
            status: "Bad request",
            message: "discount Price không hợp lệ"
        })
    }
    if(!body.duration){
        return response.status(400).json({
            status: "Bad request",
            message: "duration không hợp lệ"
        })
    }
    if(!body.level){
        return response.status(400).json({
            status: "Bad request",
            message: "level không hợp lệ"
        })
    }
    if(!body.coverImage){
        return response.status(400).json({
            status: "Bad request",
            message: "cover Image không hợp lệ"
        })
    }
    if(!body.teacherName){
        return response.status(400).json({
            status: "Bad request",
            message: "teacher Name không hợp lệ"
        })
    }
    if(!body.teacherPhoto){
        return response.status(400).json({
            status: "Bad request",
            message: "teacher Photo không hợp lệ"
        })
    }

    //B3: gọi model tạo dữ liệu mới
    const newCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending,
    }
    courseModel.create(newCourse, (error, data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create new course successfully",
            data: data
        })
    })
}

//function get all course
const getAllCourse = (request,response) =>{
    courseModel.find((error, data)=>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all course successfully",
            data: data
        })
    })
}

//function get course by id
const getCourseById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "courseId không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu
    courseModel.findById(courseId,(error, data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get detail course successfully",
            data: data
        })
    })
}

//function update voucher by id
const updateCourseById = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    const body = request.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "courseId không hợp lệ"
        })
    }
    if(!body.courseCode){
        return response.status(400).json({
            status: "Bad request",
            message: "Mã Course không hợp lệ"
        })
    }
    if(!body.courseName){
        return response.status(400).json({
            status: "Bad request",
            message: "Tên Course không hợp lệ"
        })
    }
    if(isNaN(body.price)||body.price < 0){
        return response.status(400).json({
            status: "Bad request",
            message: "Price không hợp lệ"
        })
    }
    if(isNaN(body.discountPrice)||body.discountPrice < 0){
        return response.status(400).json({
            status: "Bad request",
            message: "discount Price không hợp lệ"
        })
    }
    if(!body.duration){
        return response.status(400).json({
            status: "Bad request",
            message: "duration không hợp lệ"
        })
    }
    if(!body.level){
        return response.status(400).json({
            status: "Bad request",
            message: "level không hợp lệ"
        })
    }
    if(!body.coverImage){
        return response.status(400).json({
            status: "Bad request",
            message: "cover Image không hợp lệ"
        })
    }
    if(!body.teacherName){
        return response.status(400).json({
            status: "Bad request",
            message: "teacher Name không hợp lệ"
        })
    }
    if(!body.teacherPhoto){
        return response.status(400).json({
            status: "Bad request",
            message: "teacher Photo không hợp lệ"
        })
    }

    //B3: gọi model tạo dữ liệu
    const updateCourse = {}
    if(body.courseCode !== undefined){
        updateCourse.courseCode = body.courseCode
    }
    if(body.courseName !== undefined){
        updateCourse.courseName = body.courseName
    }
    if(body.price !== undefined){
        updateCourse.price = body.price
    }
    if(body.discountPrice !== undefined){
        updateCourse.discountPrice = body.discountPrice
    }
    if(body.duration !== undefined){
        updateCourse.duration = body.duration
    }
    if(body.level !== undefined){
        updateCourse.level = body.level
    }
    if(body.coverImage !== undefined){
        updateCourse.coverImage = body.coverImage
    }
    if(body.teacherName !== undefined){
        updateCourse.teacherName = body.teacherName
    }
    if(body.teacherPhoto !== undefined){
        updateCourse.teacherPhoto = body.teacherPhoto
    }
    if(body.isPopular !== undefined){
        updateCourse.isPopular = body.isPopular
    }
    if(body.isTrending !== undefined){
        updateCourse.isTrending = body.isTrending
    }

    courseModel.findByIdAndUpdate(courseId,updateCourse,(error,data)=>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update course successfully",
            data: data
        })
    })
}

//function delete voucher by id
const deleteCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "courseId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete course successfully"
        })
    })
}

module.exports={
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}