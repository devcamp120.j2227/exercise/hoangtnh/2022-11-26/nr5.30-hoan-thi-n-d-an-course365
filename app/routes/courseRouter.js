// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import voucher controller
const courseController = require("../controllers/courseController")

router.post("/courses", courseController.createCourse);
router.get("/courses", courseController.getAllCourse);
router.get("/courses/:courseId", courseController.getCourseById);
router.put("/courses/:courseId", courseController.updateCourseById);
router.delete("/courses/:courseId", courseController.deleteCourseById)

module.exports = router;